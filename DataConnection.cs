﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseServer
{
    class DataConnection
    {
        public static SqlConnection DataCon { get; set; }
        /// <summary>
        /// use for SQL Server Authentication
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public static void ConnectionDB(string ip,string dbName, string userName, string password)
        {
            string connectionString = "Server = " + ip + ";Database = " + dbName + ";User Id=" + userName +
            ";Password =" + password + ";";
            DataCon = new SqlConnection(connectionString);
            DataCon.Open();
                
        }
        /// <summary>
        /// use for windows authentication
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="dbName"></param>
        public static void connectionDB(string ip,string dbName)
        {
            string connectionString = "Server=" + ip + ";Database=" + dbName + ";Trusted_Connection=True;";
            DataCon = new SqlConnection(connectionString);
            DataCon.Open();
        }

    }
}
