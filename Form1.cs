﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DatabaseServer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void comboAuthentication_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = comboAuthentication.SelectedIndex;
            if (index == 0 )
            {
                txtUsername.Enabled = false;
                txtPassword.Enabled = false;
            }
            else
            {
                txtUsername.Enabled = true;
                txtPassword.Enabled = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboAuthentication.SelectedIndex=0;

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                string ip = txtIP.Text;
                string dbName = "dbsls";
                string username = txtUsername.Text;
                string password = txtPassword.Text;
                int index = comboAuthentication.SelectedIndex;

                if (index == 0)
                {
                    //windows Authentication
                    DataConnection.connectionDB(ip, dbName);
                    MessageBox.Show("successfully");

                }else
                {
                    //sql server Authentication
                    DataConnection.ConnectionDB(ip, dbName, username, password);
                }
                new MainForm().Show();
                this.Hide();

            }catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtIP_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
