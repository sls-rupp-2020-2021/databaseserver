﻿
namespace DatabaseServer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnViewPeople = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnViewPeople
            // 
            this.btnViewPeople.Location = new System.Drawing.Point(80, 58);
            this.btnViewPeople.Name = "btnViewPeople";
            this.btnViewPeople.Size = new System.Drawing.Size(233, 47);
            this.btnViewPeople.TabIndex = 0;
            this.btnViewPeople.Text = "View People";
            this.btnViewPeople.UseVisualStyleBackColor = true;
            this.btnViewPeople.Click += new System.EventHandler(this.btnViewPeople_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(353, 58);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(233, 47);
            this.button1.TabIndex = 1;
            this.button1.Text = "Lotery";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(861, 447);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnViewPeople);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnViewPeople;
        private System.Windows.Forms.Button button1;
    }
}