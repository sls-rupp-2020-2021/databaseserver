﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DatabaseServer
{
    public partial class ViewPeople : Form
    {
        public ViewPeople()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ViewPeople_Load(object sender, EventArgs e)
        {
            try
            {
                string sql = "select * from tblpeople";
                SqlCommand s = new SqlCommand(sql, DataConnection.DataCon);
                SqlDataReader r = s.ExecuteReader();

                //read each row in data table
                while(r.Read())
                {
                    string id = r.GetValue(0) + "";
                    string name = r.GetValue(1) + "";
                    string gender = r[2] + "";
                    string phoneNumber = r[3] + "";
                    string email = r[4] + "";
                    data.Rows.Add(id, name, gender, phoneNumber, email);  
                }
                r.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); 
            }
        }
    }
}
